import express from 'express';

const server = express();

server.use(express.json());

let projects = [];
let count = 1;

async function globalMiddleware(req, res, next) {
  console.log(count++);
  return next();
}

async function middleware(req, res, next) {
  const { id } = req.params;
  const project = projects.find(p => p.id == id);
  if (!project) {
    return res.status(400).json({ error: 'Projeto não existe.' });
  }
  return next();
}

server.post('/projects', globalMiddleware, (req, res) => {
  const { id, title, tasks } = req.body;
  projects.push({
    id,
    title,
    tasks,
  });
  return res.status(201).json();
});

server.post(
  '/projects/:id/tasks',
  globalMiddleware,
  middleware,
  async (req, res) => {
    const { id } = req.params;
    const { title } = req.body;
    let project = projects.find(p => p.id == id);
    project.tasks.push(title);
    return res.status(201).json(project);
  }
);

server.get('/projects', globalMiddleware, async (req, res) => {
  return await res.status(200).json(projects);
});

server.put('/projects/:id', globalMiddleware, middleware, async (req, res) => {
  const { id } = req.params;
  const { title } = req.body;
  let project = projects.find(p => p.id == id);
  project.title = title;
  return await res.status(200).json(project);
});

server.delete(
  '/projects/:id',
  globalMiddleware,
  middleware,
  async (req, res) => {
    const { id } = req.params;
    let index = projects.findIndex(p => p.id == id);
    projects.splice(index, 1);
    return res.status(204).json();
  }
);

server.listen(3030, () => {
  console.log('rodando');
});
