import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Keyboard, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';

import { 
    Container, Form, Input, SubmitButton, List,
    User, Avatar, Name, Bio, ProfileButton, ProfileButtonText } from './styles';
import api from '../../service/api';

export default class Main extends Component {

    static navigationOptions = {
        title: 'Usuários',
        
    }

    static propTypes = {
        navigation: PropTypes.shape({
            navigate: PropTypes.func,
        }).isRequired
    }

    state = {
        newUser: '',
        users: [],
        loading: false
    }

    async componentDidMount() {
        const users = await AsyncStorage.getItem('users');
        if(users){
            this.setState({ users: JSON.parse(users) });
        }
    }

    componentDidUpdate(_, prevState) {
        const { users } = this.state;
        if(prevState.users !== users){
            AsyncStorage.setItem('users', JSON.stringify(users));
        }
    }

    handleNavigate = (user) => {
        const { navigation } = this.props;
        navigation.navigate('User', { user });
    }

    handleSubmit = async () => {
        this.setState({ loading: true });
        const { users, newUser } = this.state;
        const res = await api.get(`/users/${newUser}`);
        const data = {
            name: res.data.name,
            login: res.data.login,
            bio: res.data.bio,
            avatar: res.data.avatar_url
        };
        this.setState({ users: [...users, data], newUser: '', loading: false });
        Keyboard.dismiss();
    } 

    render() {
        const { users, newUser, loading } = this.state;
        return (
            <Container >
                <Form>
                    <Input 
                        autoCorrect={false} 
                        autoCapitalize="none" 
                        placeholder="Adicionar usuário"
                        value={newUser}
                        onChangeText={text => this.setState({ newUser: text })} 
                        returnKeyType="send"
                        onSubmitEditing={this.handleSubmit}>
                    </Input>
                    <SubmitButton loading={loading} onPress={this.handleSubmit}>
                        { loading ? <ActivityIndicator color="#FFF" /> :
                        <Icon name="add" size={24} color="#FFF" /> }
                    </SubmitButton>
                </Form>
                <List 
                    data={users} 
                    keyExtractor={user => user.login} 
                    renderItem={({item}) => (
                        <User>
                            <Avatar source={{ uri:item.avatar }} />
                            <Name>{item.name}</Name>
                            <Bio>{item.bio ? item.bio : 'No bio' }</Bio>

                            <ProfileButton onPress={() => this.handleNavigate(item)}>
                                <ProfileButtonText>Ver perfil</ProfileButtonText>
                            </ProfileButton>
                            
                        </User>
                    )}/>
            </Container>
        );
    }
  
}


