import React, { Component } from 'react';
import { WebView } from 'react-native-webview';

export default class Repository extends Component {

  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('item').name
  })

  state = {
    url: ''
  }

  componentDidMount() {
    const { navigation } = this.props;
    const item = navigation.getParam('item');
    this.setState({ url: item.html_url });
  }

  render() {
    const { url, loading } = this.state;
    return <WebView source={{uri: url}} />
  }
}
