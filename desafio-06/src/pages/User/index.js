import React, { Component } from 'react';
import { ActivityIndicator } from 'react-native';

import PropTypes from 'prop-types';

import api from '../../service/api';

import { Container, Header, Avatar, Name, Bio, Stars, Starred, OwnerAvatar, Title, Info, Author } from './styles';

export default class User extends Component {

  state = {
    stars: [],
    newStars: [],
    user: {},
    loading: false,
    refreshing: false,
    page: 1
  }

  static propTypes = {
    navigation: PropTypes.shape({
      getParam: PropTypes.func,
    }).isRequired
  }

  static navigationOptions = ({ navigation }) => ({
    title: navigation.getParam('user').login
  })

  async componentDidMount() {
    const { navigation } = this.props;
    const { page } = this.state;
    const user = navigation.getParam('user');
    this.setState({ loading: true });
    const res = await api.get(`/users/${user.login}/starred?page=${page}`);
    this.setState({ stars: res.data, user, loading: false, page: page + 1 });
  }

  handleWebView = (item) => {
    const { navigation } = this.props;
    navigation.navigate('Repository', { item });
  }

  handleLoadingMore = async () => {
    const { page, user, stars } = this.state;
    const res = await api.get(`/users/${user.login}/starred?page=${page}`);

    if(res.data.length === 0){
      this.setState({ refreshing: false });
      return;
    }

    if(stars[0].id === res.data[0].id){
      this.setState({ refreshing: false });
      return;
    }
    this.setState({ stars: [...stars, ...res.data], refreshing: false });
    return;
  }

  handleReached = () => {
    this.setState({ refreshing: true, page: this.state.page + 1 }, () => {
      this.handleLoadingMore();
    })
  }

  handleRefresh = () => {
    this.setState({ refreshing: true, page: 1 }, () => {
      this.handleLoadingMore();
    })
  }

  render() {
    const { stars, user, refreshing, loading } = this.state;
    return (
      <Container>
        <Header>
          <Avatar source={{uri: user.avatar}} />
          <Name>{user.name}</Name>
          <Bio>{user.bio ? user.bio : ''}</Bio>
        </Header>

        { loading 
          ? <ActivityIndicator color="#7159c1" />
          : <Stars
              data={stars}
              onEndReachedThreshold={0.5}
              onEndReached={this.handleReached}
              onRefresh={this.handleRefresh}
              refreshing={refreshing}
              keyExtractor={star => String(star.id)}
              renderItem={({ item }) => (
            <Starred onPress={() => this.handleWebView(item)}>
              <OwnerAvatar source={{ uri: item.owner.avatar_url }} />
              <Info>
                <Title>{item.name}</Title>
                <Author>{item.owner.login}</Author>
              </Info>
            </Starred>
          )}>
        </Stars> }

      </Container>
    );
  }
  
}