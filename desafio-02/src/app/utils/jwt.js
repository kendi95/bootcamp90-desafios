import { sign, verify } from 'jsonwebtoken';
import config from '../config/auth';

export default {
  encoded(id) {
    return sign({ id }, config.secret, {
      expiresIn: config.expiresIn,
    });
  },

  decoded(token) {
    return verify(token, config.secret);
  },
};
