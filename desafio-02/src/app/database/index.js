import Sequelize from 'sequelize';
import dbConfig from '../config/database';
import Student from './models/Student';
import User from './models/User';

const models = [Student, User];

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(dbConfig);
    models.map(model => model.init(this.connection));
  }
}

export default new Database();
