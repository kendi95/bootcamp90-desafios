import Student from '../database/models/Student';

class StudentController {
  async store(req, res) {
    const student = await Student.create(req.body);
    return res.status(201).json(student);
  }
}

export default new StudentController();
