import express from 'express';
import SessionController from './app/controllers/SessionController';
import StudentController from './app/controllers/StudentController';
import auth from './app/middlewares/auth';
import AuthValidation from './app/validators/AuthValidation';
import StudentValidation from './app/validators/StudentValidation';

const routes = express.Router();

routes.post('/sessions', AuthValidation, SessionController.store);

routes.use(auth);
routes.post('/students', StudentValidation, StudentController.store);

export default routes;
