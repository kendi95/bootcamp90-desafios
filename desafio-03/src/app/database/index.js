import Sequelize from 'sequelize';
import dbConfig from '../config/database';
import Checkin from './models/Checkin';
import HelpOrder from './models/HelpOrder';
import Matricula from './models/Matricula';
import Plan from './models/Plan';
import Student from './models/Student';
import User from './models/User';

const models = [Student, User, Plan, Matricula, Checkin, HelpOrder];

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(dbConfig);
    models
      .map((model) => model.init(this.connection))
      .map(
        (model) => model.associate && model.associate(this.connection.models)
      );
  }
}

export default new Database();
