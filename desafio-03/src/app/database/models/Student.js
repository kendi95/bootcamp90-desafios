import { DataTypes, Model } from 'sequelize';

class Student extends Model {
  static init(sequelize) {
    super.init(
      {
        name: DataTypes.STRING,
        email: DataTypes.STRING,
        age: DataTypes.INTEGER,
        weight: DataTypes.INTEGER,
        height: DataTypes.INTEGER,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  static associate(models) {
    this.hasOne(models.Matricula);
    this.hasMany(models.Checkin);
    this.hasMany(models.HelpOrder);
  }
}

export default Student;
