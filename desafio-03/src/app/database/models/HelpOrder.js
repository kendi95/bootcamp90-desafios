import { DataTypes, Model } from 'sequelize';

class HelpOrder extends Model {
  static init(sequelize) {
    super.init(
      {
        question: DataTypes.TEXT,
        answer: DataTypes.TEXT,
        answer_at: DataTypes.DATEONLY,
        student_id: DataTypes.INTEGER,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Student, {
      foreignKey: 'student_id',
      as: 'student',
    });
  }
}

export default HelpOrder;
