import { DataTypes, Model } from 'sequelize';

class Matricula extends Model {
  static init(sequelize) {
    super.init(
      {
        start_date: DataTypes.DATEONLY,
        end_date: DataTypes.DATEONLY,
        price: DataTypes.DECIMAL,
        student_id: DataTypes.INTEGER,
        plan_id: DataTypes.INTEGER,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Student, {
      foreignKey: 'student_id',
      as: 'student',
    });
    this.belongsTo(models.Plan, { foreignKey: 'plan_id', as: 'plan' });
  }
}

export default Matricula;
