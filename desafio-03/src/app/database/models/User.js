import bcrypt from 'bcryptjs';
import { DataTypes, Model } from 'sequelize';

class User extends Model {
  static init(sequelize) {
    super.init(
      {
        name: DataTypes.STRING,
        email: DataTypes.STRING,
        password: DataTypes.STRING,
      },
      {
        sequelize,
      }
    );

    return this;
  }

  async checkPassword(password) {
    return await bcrypt.compare(password, this.password);
  }
}

export default User;
