import Student from '../database/models/Student';

class StudentController {
  async store(req, res) {
    const student = await Student.create(req.body);
    return res.status(201).json(student);
  }

  async update(req, res) {
    const { id } = req.params;
    await Student.update(req.body, {
      where: { id },
    });
    return res.status(204).json();
  }
}

export default new StudentController();
