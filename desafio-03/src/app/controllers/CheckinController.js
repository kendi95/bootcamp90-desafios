import { ptBR, startOfWeek } from 'date-fns';
import Checkin from '../database/models/Checkin';

class CheckinController {
  async index(req, res) {
    const { id } = req.params;
    const checkins = await Checkin.findAll({
      where: { student_id: id },
    });
    return res.status(200).json(checkins);
  }

  async store(req, res) {
    const { id } = req.params;
    try {
      const checkins = await Checkin.findAll({
        where: { student_id: id },
      });

      const now = new Date();
      let arrayCheckin = [];

      for (let index = 0; index < checkins.length; index++) {
        const created = startOfWeek(checkins[index].created_at, {
          locale: ptBR,
          weekStartsOn: 1,
        });
        const startNow = startOfWeek(now, {
          locale: ptBR,
          weekStartsOn: 1,
        });
        if (created.getTime() === startNow.getTime()) {
          arrayCheckin.push(checkins[index]);
        }
      }

      if (arrayCheckin.length >= 5) {
        return res.status(400).json({
          error: 'Não é mais possível fazer checkin derante essa semana.',
        });
      }
      const checkin = await Checkin.create({ student_id: id });
      return res.status(201).json(checkin);
    } catch (err) {
      return res.status(500).json(err);
    }
  }
}

export default new CheckinController();
