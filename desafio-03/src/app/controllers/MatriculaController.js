import Matricula from '../database/models/Matricula';

class MatriculaController {
  async index(req, res) {
    const matriculas = await Matricula.findAll({
      attributes: [
        'id',
        'start_date',
        'end_date',
        'price',
        'student_id',
        'plan_id',
      ],
    });
    return res.status(200).json(matriculas);
  }

  async store(req, res) {
    const matricula = await Matricula.create(req.body);
    return res.status(201).json(matricula);
  }

  async update(req, res) {
    const { id } = req.params;
    await Matricula.update(req.body, {
      where: { id },
    });
    return res.status(204).json();
  }

  async destroy(req, res) {
    const { id } = req.params;
    try {
      await Matricula.destroy({
        where: { id },
      });
      return res.status(204).json();
    } catch (err) {
      return res.status(500).json(err);
    }
  }
}

export default new MatriculaController();
