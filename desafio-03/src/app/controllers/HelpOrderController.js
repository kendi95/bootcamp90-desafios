import HelpOrder from '../database/models/HelpOrder';

class HelpOrderController {
  async index(req, res) {
    const orders = await HelpOrder.findAll({
      where: { answer: null },
    });
    return res.status(200).json(orders);
  }

  async show(req, res) {
    const { id } = req.params;
    const orders = await HelpOrder.findAll({
      where: { student_id: id },
    });
    return res.status(200).json(orders);
  }

  async store(req, res) {
    const { id } = req.params;
    const { question } = req.body;
    const order = await HelpOrder.create({ question, student_id: id });
    return res.status(201).json(order);
  }
}

export default new HelpOrderController();
