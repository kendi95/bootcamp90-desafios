import User from '../database/models/User';
import jwt from '../utils/jwt';

class StudentController {
  async store(req, res) {
    const { email, password } = req.body;
    const user = await User.findOne({
      where: { email },
    });

    if (!user) {
      return res.status(400).json({ error: 'Usuário não encontrado.' });
    }

    if (!user.checkPassword(password)) {
      return res.status(400).json({ error: 'Senha incorreta.' });
    }

    const token = jwt.encoded(user.id);

    const newUser = {
      name: user.name,
      token,
    };

    return res.status(200).json(newUser);
  }
}

export default new StudentController();
