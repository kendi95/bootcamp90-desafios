import Plan from '../database/models/Plan';

class PlanController {
  async index(req, res) {
    try {
      const plans = await Plan.findAll();
      return res.status(200).json(plans);
    } catch (err) {
      return res.status(500).json(err);
    }
  }

  async store(req, res) {
    const { title } = req.body;
    const ifExist = await Plan.findOne({
      where: { title },
    });
    if (ifExist) {
      return res.status(400).json({ error: 'Este plano já existe.' });
    }
    const plan = await Plan.create(req.body);
    return res.status(201).json(plan);
  }

  async update(req, res) {
    const { id } = req.params;
    await Plan.update(req.body, {
      where: { id },
    });
    return res.status(204).json();
  }

  async destroy(req, res) {
    const { id } = req.params;
    await Plan.destroy({
      where: { id },
    });
    return res.status(204).json();
  }
}

export default new PlanController();
