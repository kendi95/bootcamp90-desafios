import { parseISO } from 'date-fns';
import { format } from 'date-fns-tz';
import HelpOrder from '../database/models/HelpOrder';
import Student from '../database/models/Student';
import Mail from '../libs/Mail';

class AnswerController {
  async store(req, res) {
    const { id } = req.params;
    const { answer } = req.body;
    const order = await HelpOrder.findByPk(id);
    const student = await Student.findByPk(order.student_id);
    order.answer = answer;
    order.answer_at = new Date();
    const orderUpdated = await order.save();

    Mail.sendEmail({
      to: `${student.name} <${student.email}>`,
      subject: 'Sua questão foi respondida.',
      template: 'answer',
      context: {
        provider: `${student.name}`,
        question: order.question,
        answer,
        date: format(parseISO(order.answer_at), 'dd/MM/yyyy', {
          timeZone: 'America/Sao_Paulo',
        }),
      },
    });

    return res.status(200).json(orderUpdated);
  }
}

export default new AnswerController();
