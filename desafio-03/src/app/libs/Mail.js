import handlebars from 'express-handlebars';
import nodemailer from 'nodemailer';
import mailerHandlebars from 'nodemailer-express-handlebars';
import { resolve } from 'path';
import config from '../config/mail';

class Mail {
  constructor() {
    const { host, port, secure, auth } = config;
    this.transporter = nodemailer.createTransport({
      host,
      port,
      secure,
      auth: auth.user ? auth : null,
    });
    this.configureTemplate();
  }

  configureTemplate() {
    const viewPath = resolve(__dirname, '..', 'views', 'emails');
    this.transporter.use(
      'compile',
      mailerHandlebars({
        viewEngine: handlebars.create({
          layoutsDir: resolve(viewPath, 'layouts'),
          partialsDir: resolve(viewPath, 'partials'),
          defaultLayout: 'default',
          extname: '.hbs',
        }),
        viewPath,
        extName: '.hbs',
      })
    );
  }

  sendEmail(message) {
    return this.transporter.sendMail({
      ...config.default,
      ...message,
    });
  }
}

export default new Mail();
