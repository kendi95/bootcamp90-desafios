import * as Yup from 'yup';

const schema = Yup.object().shape({
  title: Yup.string().required(),
  duration: Yup.number().required(),
  price: Yup.number().required(),
});

export default async (req, res, next) => {
  if (!(await schema.isValid(req.body))) {
    return res.status(400).json({ error: 'Erro de validação.' });
  }
  return next();
};
