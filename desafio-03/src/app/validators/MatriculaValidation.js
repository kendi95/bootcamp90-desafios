import * as Yup from 'yup';

const schema = Yup.object().shape({
  start_date: Yup.string().required(),
  end_date: Yup.string().required(),
  price: Yup.number().required(),
  student_id: Yup.number().required(),
  plan_id: Yup.number().required(),
});

export default async (req, res, next) => {
  if (!(await schema.isValid(req.body))) {
    return res.status(400).json({ error: 'Erro de validação.' });
  }
  return next();
};
