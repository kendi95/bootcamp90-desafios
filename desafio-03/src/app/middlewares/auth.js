import jwt from '../utils/jwt';

export default async (req, res, next) => {
  const auth = req.headers.authorization;
  if (!auth) {
    return res.status(401).json({ error: 'Token é requerido.' });
  }

  const token = auth.split(' ')[1];
  const { id } = jwt.decoded(token);

  req.userId = id;
  next();
};
