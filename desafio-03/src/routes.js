import express from 'express';
import AnswerController from './app/controllers/AnswerController';
import CheckinController from './app/controllers/CheckinController';
import HelpOrderController from './app/controllers/HelpOrderController';
import MatriculaController from './app/controllers/MatriculaController';
import PlanController from './app/controllers/PlanController';
import SessionController from './app/controllers/SessionController';
import StudentController from './app/controllers/StudentController';
import auth from './app/middlewares/auth';
import AnswerValidation from './app/validators/AnswerValidation';
import AuthValidation from './app/validators/AuthValidation';
import HelpOrderValidation from './app/validators/HelpOrderValidation';
import MatriculaValidation from './app/validators/MatriculaValidation';
import PlanValidation from './app/validators/PlanValidation';
import StudentValidation from './app/validators/StudentValidation';

const routes = express.Router();

routes.post('/sessions', AuthValidation, SessionController.store);

routes.get('/help-orders', HelpOrderController.index);
routes.get('/students/:id/help-orders', HelpOrderController.show);
routes.post(
  '/students/:id/help-orders',
  HelpOrderValidation,
  HelpOrderController.store
);

routes.post(
  '/help-orders/:id/answer',
  AnswerValidation,
  AnswerController.store
);

routes.post('/students/:id/checkins', CheckinController.store);
routes.get('/students/:id/checkins', CheckinController.index);

routes.use(auth);
routes.post('/students', StudentValidation, StudentController.store);
routes.put('/students/:id', StudentValidation, StudentController.update);

routes.post('/plans', PlanValidation, PlanController.store);
routes.get('/plans', PlanController.index);
routes.put('/plans/:id', PlanValidation, PlanController.update);
routes.delete('/plans/:id', PlanController.destroy);

routes.post('/matriculas', MatriculaValidation, MatriculaController.store);
routes.put('/matriculas/:id', MatriculaValidation, MatriculaController.update);
routes.delete('/matriculas/:id', MatriculaController.destroy);
routes.get('/matriculas', MatriculaController.index);

export default routes;
