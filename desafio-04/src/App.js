import React from "react";
import "./App.css";
import avatar from "./assets/avatar.jpeg";
import Header from "./components/Header";

function App() {
	return (
		<>
			<Header />
			<ul>
				<li>
					<header>
						<nav>
							<img src={avatar} alt='Avatar' />
							<div>
								<strong>Júlio Alcântara</strong>
								<span>04 Jun 2019</span>
							</div>
						</nav>
					</header>
					<p>Pessoal, alguém sabe se a Rocketseat está contratando?</p>

					<hr />

					<div>
						<img src={avatar} alt='Avatar' />
						<div>
							<p>
								<strong>Diego Fernandes</strong>A Rocketseat está sempre em
								busca de novos membros para o time, e geralmente ficamos de olho
								em quem se destaca no Bootcamp, inclusive 80% do nosso time de
								devs é composto por alunos do Bootcamp. Além disso, se você tem
								vontade de ensinar gravando vídeos e criando posts, pode me
								chamar no Discord! (Sério, me chamem mesmo, esse comentário é
								real).
							</p>
						</div>
					</div>
				</li>

				<li>
					<header>
						<nav>
							<img src={avatar} alt='Avatar' />
							<div>
								<strong>Gabriel Lisboa</strong>
								<span>04 Jun 2019</span>
							</div>
						</nav>
					</header>
					<p>
						Fala galera beleza? <br />
						<br /> Estou fazendo o Bootcamp GoStack da Rocketseat e está sendo
						muito massa! Alguém mais fazendo, comenta na publicação para
						trocarmos uma ideia.
					</p>

					<hr />

					<div>
						<img src={avatar} alt='Avatar' />
						<div>
							<p>
								<strong>Clara Lisboa</strong>Também estou fazendo o Bootcamp e
								estou adorando! Estou no terceiro módulo sobre Node e já tenho
								minha API dos desafios construída!
							</p>
						</div>
					</div>

					<div>
						<img src={avatar} alt='Avatar' />
						<div>
							<p>
								<strong>Cézar Toledo</strong>Que maaaaassa! Estou pensando em me
								inscrever na próxima turma pra ver qual é desse Bootcamp
								GoStack, dizendo que os devs saem de lá com super poderes!
							</p>
						</div>
					</div>
				</li>
			</ul>
		</>
	);
}

export default App;
