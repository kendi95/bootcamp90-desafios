import React from "react";
import { MdAccountCircle } from "react-icons/md";
import "./styles.css";

export default function Header() {
	return (
		<header>
			<nav>
				<strong>facebook.</strong>

				<div>
					<span>Me Perfil</span>
					<MdAccountCircle size={32} color='#FFF' />
				</div>
			</nav>
		</header>
	);
}
