import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Container from '../../components/Container';
import api from '../../services/api';
import {
  Button,
  IssueList,
  Loading,
  Owner,
  PageButtons,
  SelectIssue,
} from './styles';

export default class Repository extends Component {
  static propTypes = {
    match: PropTypes.shape({
      params: PropTypes.shape({
        repository: PropTypes.string,
      }),
    }).isRequired,
  };

  state = {
    repository: {},
    issues: [],
    loading: true,
    options: [
      {
        value: 'all',
        label: 'Tudo',
      },
      {
        value: 'open',
        label: 'Aberto',
      },
      {
        value: 'closed',
        label: 'Fechado',
      },
    ],
    selectedOption: null,
    page: 1,
    disabled: true,
  };

  componentDidMount() {
    this.handleGetIssues();
  }

  handleGetIssues = async () => {
    const { match } = this.props;
    const repoName = decodeURIComponent(match.params.repository);

    const [repo, issues] = await Promise.all([
      api.get(`/repos/${repoName}`),
      api.get(`/repos/${repoName}/issues`, {
        params: {
          state: 'all',
          per_page: 30,
          page: this.state.page,
        },
      }),
    ]);

    this.setState({
      issues: issues.data,
      repository: repo.data,
      loading: false,
    });
  };

  handleSelect = async selected => {
    const { match } = this.props;
    const repoName = decodeURIComponent(match.params.repository);
    const issues = await api.get(`/repos/${repoName}/issues`, {
      params: {
        state: selected.value,
        per_page: 30,
        page: this.state.page,
      },
    });
    this.setState({
      issues: issues.data,
      loading: false,
    });
  };

  handleIncrement = () => {
    this.setState({ page: this.state.page + 1 });
    this.setState({ disabled: false });
    this.handleGetIssues();
  };

  handleDecrement = () => {
    this.setState({ page: this.state.page - 1 });
    this.handleGetIssues();
    if (this.state.page <= 1) {
      this.setState({ disabled: true });
      return;
    }
  };

  render() {
    if (this.state.loading) {
      return <Loading></Loading>;
    }
    return (
      <Container>
        <Owner>
          <Link to="/">Voltar ao repositório</Link>
          <img
            src={this.state.repository.owner.avatar_url}
            alt={this.state.repository.owner.login}
          />
          <h1>{this.state.repository.name}</h1>
          <p>{this.state.repository.description}</p>
        </Owner>

        <IssueList>
          <SelectIssue
            options={this.state.options}
            defaultValue={this.state.options[0]}
            onChange={this.handleSelect}
          />

          <PageButtons>
            <Button
              onClick={this.handleDecrement}
              disabled={this.state.disabled}
            >
              Retroceder
            </Button>
            <Button onClick={this.handleIncrement}>Avançar</Button>
          </PageButtons>

          {this.state.issues.map(issue => (
            <li key={String(issue.id)}>
              <img src={issue.user.avatar_url} alt={issue.user.login} />
              <div>
                <strong>
                  <a href={issue.html_url}>{issue.title}</a>
                  {issue.labels.map(label => (
                    <span key={String(label.id)}>{label.name}</span>
                  ))}
                </strong>
                <p>{issue.user.login}</p>
              </div>
            </li>
          ))}
        </IssueList>
      </Container>
    );
  }
}
